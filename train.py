from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf

import os
import random
import pathlib

AUTOTUNE = tf.data.experimental.AUTOTUNE

DATA_PATH = os.environ['DATA_PATH']
MODEL_PATH = os.environ['MODEL_PATH']

data_root = DATA_PATH

all_image_paths = data_root.glob('*/*')

all_image_paths = [str(path) for path in all_image_paths]
random.shuffle(all_image_paths)

label_to_index = {
    'not_anime': 0,
    'anime': 1
}

all_image_labels = [label_to_index[pathlib.Path(path).parent.name]
                    for path in all_image_paths]


def preprocess_image(image):
  image = tf.image.decode_jpeg(image, channels=3)
  image = tf.image.resize(image, [192, 192])
  image /= 255.0  # normalize to [0,1] range

  return image

def load_and_preprocess_image(path):
  image = tf.io.read_file(path)
  return preprocess_image(image)

image_path = all_image_paths[0]
label = 1

path_ds = tf.data.Dataset.from_tensor_slices(all_image_paths)
image_ds = path_ds.map(load_and_preprocess_image, num_parallel_calls=AUTOTUNE)

label_ds = tf.data.Dataset.from_tensor_slices(tf.cast(all_image_labels, tf.int64))

image_label_ds = tf.data.Dataset.zip((image_ds, label_ds))

print(image_ds)
print(all_image_labels)

BATCH_SIZE = 32

# Setting a shuffle buffer size as large as the dataset ensures that the data is
# completely shuffled.
ds = image_label_ds.shuffle(buffer_size=len(all_image_paths))
ds = ds.repeat()
ds = ds.batch(BATCH_SIZE)
# `prefetch` lets the dataset fetch batches, in the background while the model is training.
ds = ds.prefetch(buffer_size=AUTOTUNE)

print(ds)

'''

'''

layers = tf.keras.layers

model = tf.keras.Sequential()

model.add(layers.Conv2D(32, kernel_size=6, activation='relu', input_shape=(192, 192, 3)))
model.add(layers.MaxPooling2D(pool_size=(2,2)))
model.add(layers.BatchNormalization())

model.add(layers.Conv2D(64, kernel_size=3, activation='relu'))
model.add(layers.MaxPooling2D(pool_size=(2,2)))
model.add(layers.BatchNormalization())

model.add(layers.Conv2D(74, kernel_size=3, activation='relu'))
model.add(layers.MaxPooling2D(pool_size=(2,2)))
model.add(layers.BatchNormalization())

model.add(layers.Conv2D(92, kernel_size=6, activation='relu'))
model.add(layers.MaxPooling2D(pool_size=(2,2)))
model.add(layers.BatchNormalization())

model.add(layers.Conv2D(32, kernel_size=6, activation='relu'))
model.add(layers.MaxPooling2D(pool_size=(2,2)))
model.add(layers.BatchNormalization())

model.add(layers.Dropout(0.2))

model.add(layers.Flatten())
model.add(layers.Dense(128, activation='relu'))
# model.add(layers.Dense(64, activation='relu'))
model.add(layers.Dense(48, activation='relu'))
model.add(layers.Dropout(0.2))
model.add(layers.Dense(1, activation='sigmoid'))

model.compile(optimizer='adam',
              loss='binary_crossentropy',
              metrics=['binary_accuracy'])

model.summary()

history = model.fit(ds, epochs=10, steps_per_epoch=100)

# tf.keras.utils.plot_model(
#     model,
#     to_file='model.png',
#     show_shapes=False,
#     show_layer_names=True,
#     rankdir='TB'
# )

print(history.history)

model.save(MODEL_PATH)

import pickle
with open('history.pickle', 'wb') as f:
  pickle.dump(history.history, f)
