import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

import sys

model = tf.keras.models.load_model('model.h5')

def preprocess_image(image):
  image = tf.image.decode_jpeg(image, channels=3)
  image = tf.image.resize(image, [192, 192])
  image /= 255.0  # normalize to [0,1] range

  return image

def load_and_preprocess_image(path):
  image = tf.io.read_file(path)
  return preprocess_image(image)

img = load_and_preprocess_image(sys.argv[1])
img = tf.expand_dims(img, 0)

res = model.predict(img)
print(res)

plt.imshow(mpimg.imread(sys.argv[1]))
plt.title('Anime? ' + str(round(res[0][0] * 100)) + '%')
plt.show()

