import telebot 
import requests
import tensorflow as tf
import logging
import os

logging.basicConfig(level=logging.INFO)

BOT_TOKEN = os.getenv('BOT_TOKEN')
MODEL_PATH = os.getenv('MODEL_PATH', './model')

bot = telebot.TeleBot(BOT_TOKEN)

def init_model(model_path):
    model = tf.keras.models.load_model(model_path)
    return model

def preprocess_image(image):
  image = tf.image.resize(image, [192, 192])
  image /= 255.0  # normalize to [0,1] range

  image = tf.expand_dims(image, 0)

  return image

def download_image(file_id):
    file_info = bot.get_file(file_id)
    res = requests.get('https://api.telegram.org/file/bot{0}/{1}'.format(BOT_TOKEN, file_info.file_path))
    print(res)
    data = tf.io.decode_image(res.content, channels=3)
    # image = tf.cast(data, tf.float32)
    return data

logging.info(f'loading model at {MODEL_PATH}')
model = init_model(MODEL_PATH)

@bot.message_handler(content_types=['photo'])
def handle_image(message):
    logging.info(f'received image {message.photo[0].file_id} from {message.from_user.username}')

    image = download_image(message.photo[0].file_id)
    image = preprocess_image(image)
    
    logging.debug('processed image')

    res = model.predict(image)
    percent = round(res[0][0] * 100)
    logging.info(f'result for {message.photo[0].file_id} is {percent}')
    bot.reply_to(message, f'Anime? {percent}%')

@bot.message_handler(commands=['start', 'help'])
def handle_start(message):
    logging.info(f'received /start: {message.from_user.username}')
    bot.reply_to(message, 'Hi! Send me a picture and I will tell you how Anime it is.')

logging.info('Launching...')
bot.polling()